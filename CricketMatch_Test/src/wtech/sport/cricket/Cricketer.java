/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wtech.sport.cricket;

import wtech.sport.Player;

/**
 *
 * @author lmangoua
 */
public class Cricketer extends Player {
    
    //Instance declaration
    private int highestScore, totalRuns, noOfInnings, currentScore;
    
    //Constructor
    public Cricketer(String name, String surname, int highestScore, int totalRuns, int noOfInnings) {
        
        super(name, surname); //from the superclass "Player"
        this.highestScore = highestScore;
        this.totalRuns = totalRuns;
        this.noOfInnings = noOfInnings;
    }
    
    //Constructor for players making their debut with "0" stats
    public Cricketer(String name, String surname) {
        
        this(name, surname, 0, 0, 0);
    }
    
    public void bat() {
        
        int initialCurrentScore = highestScore + 50;
        currentScore = (int) (initialCurrentScore * Math.random());
        
        System.out.println(super.getName() + " " + super.getSurname() + " scored " + currentScore);
        
        if (currentScore > highestScore) {
            highestScore = currentScore;
            totalRuns += currentScore;
            noOfInnings++;
        }   
    }
    
    public void printDetails() {
        
        double aver;
        int ave;
        
        aver = (double) (totalRuns/noOfInnings);
        ave = (int)aver;
        
        System.out.println(super.getName() + " " + super.getSurname() + " scored " + currentScore + " today.");
        System.out.println("Battling average is now: " + ave);
    }
    
    //Overridin abstract method Play() from superclass "Player"   
    public void Play(){
         
        bat();
    }
}
