
import wtech.sport.cricket.Bowler;
import wtech.sport.cricket.Cricketer;
import wtech.sport.cricket.Keeper;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lmangoua
 */
public class CricketMatch {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Cricketer instance creation
        Cricketer c1 = new Cricketer("Dave", "Matthews", 151, 2073, 41);
        Cricketer c2 = new Cricketer("Jon", "Bon Jovi", 87, 3896, 60);
        Cricketer c3 = new Cricketer("Ben", "Affleck", 121, 3211, 52);
        Cricketer c4 = new Cricketer("Matt", "Damon");
        Cricketer c5 = new Cricketer("Roman", "Keating");
        
        //Keeper instance creation
        Keeper k1 = new Keeper("Steven", "Soderbergh");
        
        //Bowler instance creation
        Bowler b1 = new Bowler("Mattew", "Perry", 50, 352, 12, 24);
        Bowler b2 = new Bowler("Larry", "Wachowski", 41, 191, 12, 29);
        Bowler b3 = new Bowler("Brad", "Pitt", 33, 250, 20, 44);
        Bowler b4 = new Bowler("Harrison", "Ford", 25, 87, 8, 19);
        Bowler b5 = new Bowler("Steven", "Segal");
        
        //Declaration & Instantiation array of type Cricketer
        Cricketer[] team = {c1, c2, c3, c4, c5, k1, b1, b2, b3, b4, b5};

//        Cricketer[] team = new Cricketer[11];
        
        System.out.println("Playing a match: ");

//        //loop & call Play() for every element
        for (int i = 0; i < team.length; i++) {
            team[i].Play();
        }
        
        System.out.println("");
        System.out.println("Summary: ");
        System.out.println("");

        //loop & call printDetails() for every element
        for (int i = 0; i < team.length; i++) {

            team[i].printDetails();
            System.out.println("");
        }

    }
}
